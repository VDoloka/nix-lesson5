package org.nix;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import java.time.LocalDate;

class CarTest {

    @Test
    void testCheckPassed() {
        Car car = Car.builder()
                .checkDate(LocalDate.of(2021, 10, 26))
                .build();
        Assertions.assertTrue(car.checkPassed());
           car.setCheckDate(LocalDate.of(2018, 10, 26));
        Assertions.assertFalse(car.checkPassed());
    }

    @Test
    void testMaxDistanceOnFullTank() {
        Car car = Car.builder()
                .fuelCostPer100km(11)
                .fuelTankCapacity(55.5)
                .build();
        Assertions.assertEquals(504.55,car.maxDistanceOnFullTank(),0.1);
    }

    @Test
    void testCanDrivesUp() {
        Car car = Car.builder()
                .fuelTankCapacity(55)
                .build();
        Assertions.assertTrue(car.canDrivesUp());
        car.setFuelTankCapacity(0);
        Assertions.assertFalse(car.canDrivesUp());
    }
}