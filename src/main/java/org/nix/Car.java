package org.nix;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;
import java.time.Period;

@Data
@Builder
public class Car {

    private double fuelTankCapacity;
    private double fuelCostPer100km;
    private LocalDate checkDate;


    boolean canDrivesUp() {
        return (fuelTankCapacity > 0);
    }

    boolean checkPassed() {
        return (Period.between(checkDate, LocalDate.now()).getYears() < 2);
    }

    double maxDistanceOnFullTank() {
        return fuelTankCapacity / fuelCostPer100km * 100;
    }
}