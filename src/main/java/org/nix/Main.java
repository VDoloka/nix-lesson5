package org.nix;

import java.time.LocalDate;

public class Main {
    public static void main(String[] args) {
        Car car = Car.builder()
                .fuelCostPer100km(11)
                .fuelTankCapacity(55.5)
                .checkDate(LocalDate.of(2019, 10, 26))
                .build();
        System.out.println(car);
        System.out.println("Can drive up: "+ car.canDrivesUp());
        System.out.println("Check Passed: " + car.checkPassed());
        System.out.print("Max distance : ");
        System.out.printf("%.2f", car.maxDistanceOnFullTank());
    }
}